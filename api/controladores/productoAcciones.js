'use strict';

var mongoose = require('mongoose'),
    Productos = mongoose.model('Productos');

exports.listaProductos = function(req, res) {
    Productos.find({}, 'nombre codigo cantidad ', function(err, productos) {
        if (err)
            res.send(err);
        res.json(productos);
    });
};