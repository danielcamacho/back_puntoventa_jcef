'use strict';
module.exports = function(app) {
    var controladorProducto = require('../controladores/productoAcciones');

    // profiles Routes
    app.route('/productos')
        .get(controladorProducto.listaProductos);
};