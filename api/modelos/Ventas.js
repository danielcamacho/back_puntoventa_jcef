'use strict';
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Ventas = new Schema({
    no_venta: {
        type: Number,
        default: 0
    },
    producto_vendido: {
        type: String,
        required: 'introduzca nombre del producto'
    },
    fecha_venta: {
        type: Date,
        required: 'introduzca fecha'
    },
    usuario_que_vendio: {
        type: Schema.ObjectId,
        ref: "Usuarios"
    }
});

module.exports = mongoose.model('Ventas', Ventas);