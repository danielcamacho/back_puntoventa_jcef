'use strict';
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Usuarios = new Schema({
    id: {
        type: Number,
        required: 'introduzca el identificador'
    },
    Nombre: {
        type: String,
        required: 'introduzca nombre'
    },
    correo_electronico: {
        type: String,
        required: 'introduzca el e-mail'
    },
    telefono: {
        type: String,
        required: 'introduzca telefono'
    },
    contraseña: {
        type: String,
        required: 'introduzca contraseña'
    },
});

module.exports = mongoose.model('Usuarios', Usuarios);