'use strict';
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var productosDatos = new Schema({
    identificador: {
        type: Number,
        required: 'introduzca identificador'
    },
    producto: {
        type: Schema.ObjectId,
        ref: "Productos"
    },
    codigo_de_barras: {
        type: String,
        required: 'introduzca codigo'
    },
    cantidad: {
        type: Number,
        required: 'introduzca numero'
    }
});

module.exports = mongoose.model('productosDatos', productosDatos);