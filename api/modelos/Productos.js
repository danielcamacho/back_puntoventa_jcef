'use strict';
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Productos = new Schema({
    codigo: {
        type: String,
        default: 'sincodigo'
    },
    nombre: {
        type: String,
        required: 'introduzca el nombre'
    },
    cantidad: {
        type: Number,
        default: 3
    }
});

module.exports = mongoose.model('Productos', Productos);