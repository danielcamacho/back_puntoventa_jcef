const express = require('express');
const mongoose = require('mongoose');
const app = express();
const Productos = require('./api/modelos/Productos');
const Usuarios = require('./api/modelos/Usuarios');
const productos_datos = require('./api/modelos/productos_datos');
const Ventas = require('./api/modelos/Ventas');
const rutasProducto = require('./api/rutas/rutasProducto');
const cors = require('cors');

var corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

mongoose.Promise = global.Promise;
mongoose.connect(
    'mongodb://localhost:27017/baseDeMongoose', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    },
    function(err) {
        if (err) throw err;
        console.log('Estas conectado!');
    }
);

app.use(cors(corsOptions));

rutasProducto(app);

app.listen(3000, () => {
    console.log("iniciado puerto 3000!");
});